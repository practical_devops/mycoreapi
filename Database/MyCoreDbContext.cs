﻿using Microsoft.EntityFrameworkCore;
using MyCoreApi.Models;

namespace MyCoreApi.Database
{
    public class MyCoreDbContext : DbContext
    {
        public MyCoreDbContext(DbContextOptions<MyCoreDbContext> options) : base(options) { }

        public DbSet<Note> Notes { get; set; }
    }
}