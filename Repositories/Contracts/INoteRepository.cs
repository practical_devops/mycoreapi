﻿using MyCoreApi.Models;
using System.Collections.Generic;

namespace MyCoreApi.Repositories.Contracts
{
    public interface INoteRepository
    {
        Note FindNoteById(long id);

        IEnumerable<Note> GetAllNotes();

        Note SaveNote(Note note);

        Note EditNote(Note note);

        void DeleteNote(Note note);
    }
}