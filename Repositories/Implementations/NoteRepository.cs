﻿using MyCoreApi.Database;
using MyCoreApi.Models;
using MyCoreApi.Repositories.Contracts;
using System.Collections.Generic;

namespace MyCoreApi.Repositories.Implementations
{
    public class NoteRepository : INoteRepository
    {
        private readonly MyCoreDbContext _context;

        public NoteRepository(MyCoreDbContext context)
        {
            _context = context;
        }

        public void DeleteNote(Note note)
        {
            _context.Notes.Remove(note);
            _context.SaveChanges();
        }

        public Note EditNote(Note note)
        {
            _context.Notes.Update(note);
            _context.SaveChanges();
            return note;
        }

        public Note FindNoteById(long id)
        {
            return _context.Notes.Find(id);
        }

        public IEnumerable<Note> GetAllNotes()
        {
            return _context.Notes;
        }

        public Note SaveNote(Note note)
        {
            _context.Notes.Add(note);
            _context.SaveChanges();
            return note;
        }
    }
}
