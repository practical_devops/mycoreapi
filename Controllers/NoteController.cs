﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyCoreApi.Models;
using MyCoreApi.Repositories.Contracts;

namespace MyCoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoteController : ControllerBase
    {
        private readonly INoteRepository _noteRepository;

        public NoteController(INoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Note>> GetNotes()
        {
            return _noteRepository.GetAllNotes().ToList();
        }

        [HttpGet("{noteId}")]
        public ActionResult<Note> GetNote(long noteId)
        {
            return _noteRepository.FindNoteById(noteId);
        }

        [HttpPost]
        public ActionResult<Note> PostNote(Note note)
        {
            var currentDate = DateTime.Now;
            note.CreatedDate = currentDate;
            note.LastModified = currentDate;

            return _noteRepository.SaveNote(note);
        }

        [HttpPut]
        public ActionResult<Note> PutNote(Note note)
        {
            note.LastModified = DateTime.Now;

            return _noteRepository.EditNote(note);
        }

        [HttpDelete("{noteId}")]
        public ActionResult<Note> DeleteNote(long noteId)
        {
            var note = _noteRepository.FindNoteById(noteId);
            _noteRepository.DeleteNote(note);
            return note;
        }
    }
}